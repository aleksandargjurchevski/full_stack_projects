<a name="readme-top"></a>

<div align="center">
  <a href="#">
    <img src="./images/Brainster-logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">My Portfolio from the Full-Stack Academy</h3>

</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#build-with">Build with</a>
    </li>
    <li>
      <a href="#about-the-project">About the project</a>
    </li>
    <li>
      <a href="#contact">Contact</a>
    </li>
  </ol>
</details>

<h3 id="build-with">Build with:</h3>

<div>
Below, you'll find a comprehensive list of the programming languages that we'll be covering at the Brainster Full-Stack Academy. Each project will be developed using these languages.
</div>
<br>
<ul>
  <!-- https://shields.io/badges for creating readme file badges -->
  <li><img alt="HTML" src="https://img.shields.io/badge/-HTML5-e34c26?logo=html5&logoColor=white"/></li>
  <li><img alt="CSS" src="https://img.shields.io/badge/-CSS3-264de4?logo=css3&logoColor=white"/></li>
  <li><img alt="SASS" src="https://img.shields.io/badge/-SASS-CD6799?logo=sass&logoColor=white"/></li>
  <li><img alt="Bootstrap" src="https://img.shields.io/badge/-Bootstrap-CD6799?logo=bootstrap&logoColor=white"/></li>
  <li><img alt="Javascript" src="https://img.shields.io/badge/-Javascript-EFD81D?logo=javascript&logoColor=white"/></li>
  <li><img alt="Php" src="https://img.shields.io/badge/-Php-777BB4?logo=php&logoColor=white"/></li>
  <li><img alt="Laravel" src="https://img.shields.io/badge/-Laravel-FF2D20?logo=laravel&logoColor=white"/></li>
</ul>

<h3 id="about-the-project">About the projects</h3>
<div>
This repo encompasses the array of challenges I have tackled and will continue to undertake as a student at the Brainster Full-Stack Academy. Each challenge is associated with its own branch, reflecting my journey of growth and learning.</div>
<div>
<br>
<h3 id="contact">Contact:</h3>
<ul>
    <li>Email: aleksandargjurchevski@gmail.com</li>
    <li>Telephone: +38972271779</li>
    <li><a href="https://www.linkedin.com/in/aleksandargjurchevski/">Linkedin profile</a></li>
    <li><a href="https://gitlab.com/aleksandargjurchevski/">Gitlab profile</a></li>
</ul>

<div align="right">
[back to top &#8593;](#readme-top)
</div>
